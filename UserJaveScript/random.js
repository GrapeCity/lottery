var page = Forguncy.Page;
var str = page.getCell("string");
var s = 0;
var _arr = 0;
var _start = 0;
var _end = 0;
var _size = 0;

function newNumber(start,end){  
    return Math.round(Math.random()*(end-start)+start);
}  

function isHaveThisNumber(para,num){  
    for(var i=0;i<_arr.length;i++){  
        if(_arr[i]==num){  
            return true;
        }  
    }   
    if(typeof(para) == "object")  
    {  
        if(para.length==0)  
        {  
            return false;  
        }  
    } 
    for(var i=0;i<para.length;i++){  
        if(para[i]==num){  
            return true;  
        }  
    }

    return false;  
}  

function newRandomNumbersWithNoRepeat(){  
    var para=new Array();
    var rnum;
    var currentIndex=0;
    if(_start>_end||_start<0||_end<0||_size<0){  
        return;  
    }  
    if(_end-_start+1<_size){   
        return;  
    }  
    for(var i=0;i<_size;i++){ 
        rnum=newNumber(_start,_end);
        if(isHaveThisNumber(para,rnum)){ 
            while(isHaveThisNumber(para,rnum)){  
                rnum=newNumber(_start,_end);
            }  
        }  
        para[currentIndex++]=rnum;
    }  
    str.setValue(para.join(","));
  
}

function run(start,end,size,arr){
    if (arr != null){
        _arr = arr.split(",")
    }
    
    _start = start;
    _end = end;
    _size = size;
    s = window.setInterval(newRandomNumbersWithNoRepeat, 50);
}

function stop(){
    window.clearInterval(s)
}